%% init files and reader
% OCENA
% roc analysis
% introduction to ml - testowanie hipotez binarnych
% demsar journal of ml research
% garcia herrer
clear all;

% ADS 
vs = VideoSampler('ads_dataset');
fileID = fopen('ads_dataset.txt','w');
contentType = 'Commercial';
% MOVIES
% vs = VideoSampler('movies_dataset');
% fileID = fopen('movies_dataset.txt','w');
% contentType = 'Movie';
% test
% vs = VideoSampler('dataset_test');
% fileID = fopen('dataset_test.txt','w');
% contentType = 'test';

%% init variables
edgeChanges = [];
bhattacharyyaChanges = [];
colorIntensity = [];

i = 1;
hardcuts = 0;
prevFrame = [];
prevHist = [];
histogr = [];
frame = [];
sample_index = 1;

while true
    %% get new frame
    prevHist = histogr;
    prevFrame = frame;
    [endOfSet, endOfFile, frame, frameRate] = vs.getNextFrame();
    histogr = getHistogram(frame, 64);
    if endOfSet == true
        break;
    end
    if isempty(prevFrame)
        continue;
    end
    %% extract features
    ecr = edgeChangeRatio(prevFrame, frame);
    bha = bhattacharyya(histogr, prevHist);
    int = mean(frame(:));
    if bha > 0.4
        hardcuts = hardcuts + 1;
    end
    %% save to arrays
    edgeChanges(1,i) = ecr;
    bhattacharyyaChanges(1,i) = bha;
    colorIntensity(1,i) = int;
    %% save sample to file
    if endOfFile
        % plotuj edge changes
        createfigure(edgeChanges(1:300), ...
            strjoin({strjoin({contentType,'sample'}), ...
            num2str(sample_index)}), ...
            'Edge Change Ratio', ...
            1);
        % plotuj bhattachryya changes
        createfigure(bhattacharyyaChanges(1:300), ...
            strjoin({strjoin({contentType,'sample'}), ...
            num2str(sample_index)}), ...
            'Bhattacharyya distance ratio', ...
            1);
        % plotuj color intensity ratio
        createfigure(colorIntensity(1:300), ...
        strjoin({strjoin({contentType,'sample'}), ...
        num2str(sample_index)}), ...
        'Color intensity ratio', ...
        255);
        % 
        action_mean = mean(edgeChanges);
        action_std = std(edgeChanges);
        hardcutsRatio = hardcuts * frameRate / i;
        avgColorInt = mean(colorIntensity);
        stdColorInt = std(colorIntensity);
        fprintf(fileID, '%f, %f, %f, %f, %f, %s\r\n', ...
            action_mean, action_std, hardcutsRatio, avgColorInt, stdColorInt, contentType);
        hardcuts = 0;
        i = 1;
        sample_index = sample_index + 1;
    else
        i = i+1;
    end
end

fclose(fileID);