%% init
clear all;
vs = VideoSampler('test_set');

[endOfVideo, frame] = vs.getNextFrame();
histCurrent = getHistogram(frame, 64);
%% prepare animated lines
edgeChangesWatcher = animatedline('Color','r');
bhattacharyyaWatcher = animatedline('Color','b');
axis([0 5 0 1])
legend('Edge Change Ratio', 'Histogram Difference');
x = linspace(0,5,125);
i = 1;
hardcuts = 0;
while true
    %% get new frame
    prevHist = histCurrent;
    prevFrame = frame;
    [endOfVideo, frame] = vs.getNextFrame();
    if endOfVideo == true
        break;
    end
    %% extract features
    histCurrent = getHistogram(frame, 64);
    ecr = edgeChangeRatio(prevFrame, frame);
    bha = bhattacharyya(histCurrent, prevHist);
    if bha > 0.3
        hardcuts = hardcuts + 1;
    end
    %% update view
    edgeChangesWatcher.addpoints(x(i),ecr);
    bhattacharyyaWatcher.addpoints(x(i),bha);
    drawnow limitrate
    %% plot changes
    if i == 125
        i = 1;
        [~, ecr_points] = edgeChangesWatcher.getpoints();
        action_mean = mean(ecr_points);
        action_std = std(ecr_points);
        edgeChangesWatcher.clearpoints();
        bhattacharyyaWatcher.clearpoints();
        %% print features
        fprintf('action mean: %f, action std: %f, hardcuts: %d\n', action_mean, action_std, hardcuts);
        hardcuts = 0;
    else
        i = i+1;
    end
end
drawnow
