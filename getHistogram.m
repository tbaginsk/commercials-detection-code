function [ result ] = getHistogram( frame, bins )
%getHistogram returns histogram from frame an
[x,y] = size(frame);
frame = reshape(frame, x*y, 1);
% newHistogram = histogram(frame,linspace(0,255,bins+1),'Normalization','probability');
% result = newHistogram.Values;
[f,~] = imhist(frame, bins);
f = f/sum(f);
result = f';
end

