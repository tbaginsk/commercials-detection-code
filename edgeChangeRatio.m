function [ value ] = edgeChangeRatio( frame1, frame2 )
%edgeChangeRatio computes edge change ratio for two images
frame1 = medfilt2(frame1,[3 3]); % smooth the image
[frame1, ~] = imgradient(frame1);% compute gradient
edge1 = edge(frame1);
edgePx1 = sum(edge1(:));
frame2 = medfilt2(frame2,[3 3]); % smooth the image
[frame2, ~] = imgradient(frame2);% compute gradient
edge2 = edge(frame2);
edgePx2 = sum(edge2(:));
diff = getMatChange(edge1, edge2);

[w1,k1] = size(edge1);
[w2,k2] = size(edge2);
if w1 <= w2
    w = w1;
else
    w = w2;
end
if k1 <= k2
    k = k1;
else
    k = k2;
end

enteringPx = 0;
exitingPx = 0;
for i = 1:w
    for j = 1:k
        % potential entering pixel
        if diff(i,j) == 1
            smat = getSubmatrix(edge1,i,j,2);
            if sum(smat(:)) == 0
                enteringPx = enteringPx + 1;
            end
        %potential exiting pixel
        elseif diff(i,j) == -1
            smat = getSubmatrix(edge2,i,j,2);
            if sum(smat(:)) == 0
                exitingPx = exitingPx + 1;
            end
        end
    end
end

value = max([enteringPx/(edgePx2), exitingPx/(edgePx1)]);

end

