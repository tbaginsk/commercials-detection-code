function [ result ] = getMatChange( mat1, mat2 )
%getMatDiff returns matrix change, mat1 and mat2 contain only 0s and 1s
% 0 if pixel doesnt change
% -1 if exits
% 1 if enters
[w1,k1] = size(mat1);
[w2,k2] = size(mat2);
if w1 <= w2
    w = w1;
else
    w = w2;
end
if k1 <= k2
    k = k1;
else
    k = k2;
end
result = zeros(w,k);
for i = 1:w
    for j = 1:k
        % entering pixel
        if mat2(i,j) > mat1(i,j)
            result(i,j) = 1;
        elseif mat2(i,j) < mat1(i,j)
            result(i,j) = -1;
        end
    end
end

end

