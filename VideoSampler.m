classdef VideoSampler
    %VideoSampler A class responsible for sampling input video
    %   Class treats *.mp4 videos as a video straeam and returns
    %   consecutive 5 second long samples. The sample contains 
    %   average intensitiy of the frames.
    
    properties
        folder
        files
    end
    
    methods
        function obj = VideoSampler(folderName)
            obj.folder = folderName;
            path = sprintf('%s\\*.mp4', obj.folder);
            obj.files = dir(path);
        end
        function [endOfVideo, nextSample] = getNextSample(obj)
            frameIndex = 1;
            nextSample = zeros(125,64);
            endOfVideo = false;
            persistent FILE_INDEX;
            if isempty(FILE_INDEX)
                FILE_INDEX = 1;
            end
            persistent VIDEO_F_READER;                
            for i = FILE_INDEX : length(obj.files)
                if isempty(VIDEO_F_READER)
                    file = obj.files(FILE_INDEX);
                    disp(file.name)
                    path = sprintf('%s\\%s', obj.folder, file.name);
                    VIDEO_F_READER = vision.VideoFileReader(path);
                end
                while ~isDone(VIDEO_F_READER)
                    frame = VIDEO_F_READER.step();
                    frame = rgb2gray(frame);
                    [x,y] = size(frame);
                    frame_vec = reshape(frame, x*y, 1);
                    nextSample(frameIndex,:) = hist(frame_vec,64);
                    frameIndex = frameIndex + 1;
                    if frameIndex > 125
                        return
                    end
                end
                release(VIDEO_F_READER);
                VIDEO_F_READER = [];
                FILE_INDEX = FILE_INDEX + 1;
            end
            endOfVideo = true;
        end
        function [endOfSet, endOfFile, nextFrame, frameRate] = getNextFrame(obj)
            endOfSet = false;
            endOfFile = false;
            frameRate = 0;
            nextFrame = [];
            persistent FILE_INDEX;
            if isempty(FILE_INDEX)
                FILE_INDEX = 1;
            end
            persistent VIDEO_F_READER;
            persistent FRAME_RATE;
            for i = FILE_INDEX : length(obj.files)
                if isempty(VIDEO_F_READER)
                    file = obj.files(FILE_INDEX);
                    disp(file.name)
                    path = sprintf('%s\\%s', obj.folder, file.name);
                    VIDEO_F_READER = vision.VideoFileReader(path);
                    VIDEO_F_READER.ImageColorSpace = 'Intensity';
                    VIDEO_F_READER.VideoOutputDataType = 'uint8';
                    S = VIDEO_F_READER.info();
                    FRAME_RATE = S.VideoFrameRate;
                end
                if ~isDone(VIDEO_F_READER)
                    frameRate = FRAME_RATE;
                    nextFrame = VIDEO_F_READER.step();
                    if isDone(VIDEO_F_READER)
                        endOfFile = true;
                    end
                    return
                end
                release(VIDEO_F_READER);
                VIDEO_F_READER = [];
                FILE_INDEX = FILE_INDEX + 1;
            end
            endOfSet = true;
        end
    end
    
end

