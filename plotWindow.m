function [] = plotWindow( differences, edgeChanges, meansDiff )
subplot(3,1,1);
plot(linspace(0,5,125), differences)
axis([0 5 0 1])

subplot(3,1,2);
plot(linspace(0,5,125), edgeChanges)
axis([0 5 0 1])

subplot(3,1,3);
plot(linspace(0,5,125), meansDiff)
axis([0 5 0 125])

end

