%% Clear
clear all

%% Load data from files and
% ads = load('output.mat');
% talkShow = load('out_lis.mat');

%% Extract features and form table
% adsFeatures = zeros(length(ads.output), 4);
% for i = 1:length(ads.output)
%     adsFeatures(i,1:3) = extractFeatures(ads.output(i,:));
%     adsFeatures(i,4) = 1;
% end
% 
% talkShowFeatures = zeros(length(talkShow.output), 4);
% for i = 1:length(talkShow.output)
%     talkShowFeatures(i,1:3) = extractFeatures(talkShow.output(i,:));
%     talkShowFeatures(i,4) = 0;
% end
% 
% T = table([adsFeatures(:,1);talkShowFeatures(:,1)],...
%     [adsFeatures(:,2);talkShowFeatures(:,2)],...
%     [adsFeatures(:,3);talkShowFeatures(:,3)],...
%     [adsFeatures(:,4);talkShowFeatures(:,4)],...
%     'VariableNames',{'mean', 'std', 'sumDiff', 'type'});

%% load dataset
% dataset = importfile1('dataset2.txt', 1, 70);
dataset = importfile2('dataset3.txt', 1, 70);

%% Run classificationLearner
classificationLearner