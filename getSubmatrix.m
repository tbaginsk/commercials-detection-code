function [ smat ] = getSubmatrix( mat, y, x, r )
%getSubmatrix returns a square submatrix of given matrix
%   x, y are coordinates of the submatrix center
%   r is length of the square's edge / 2, must be > 0

[w,k] = size(mat);
row_start = y - r;
if row_start < 1
    row_start = 1;
end
row_end = y + r;
if row_end > w
    row_end = w;
end
col_start = x - r;
if col_start < 1
    col_start = 1;
end
col_end = x + r;
if col_end > k
    col_end = k;
end

smat = mat(row_start:row_end, col_start:col_end);

end

